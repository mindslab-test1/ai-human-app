import 'package:app/controllers/auto_complete_controller.dart';
import 'package:app/controllers/stomp_controller.dart';
import 'package:app/utils/debouncer.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AutoCompleteMain extends StatelessWidget {
  const AutoCompleteMain({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    AutoCompleteController autoCompleteController = Get.put(AutoCompleteController());
    final _debouncer = Debouncer(milliseconds: 500);

    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Container(
          height: 60,
          width: 200,
          child: TextField(
            controller: autoCompleteController.textController,
            onChanged: (String val) {
              _debouncer.run(() => Get.put(StompController()).sendAutoComplete(val));
            },
          ),
        ),
        SizedBox(
          width: 100,
        ),
        Obx(
            () => SizedBox(
              height: 100,
              width: 700,
              child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: autoCompleteController.autoCompleteList.length,
                  itemBuilder: (context, index) {
                    return Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Container(
                        color: Colors.black,
                        child: TextButton(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                                autoCompleteController.autoCompleteList[index].text,
                                style: TextStyle(color: Colors.white),
                            ),
                          ),
                          onPressed: () {
                            Get.put(StompController())
                                .sendTextToStomp(autoCompleteController.autoCompleteList[index].text);
                          },
                        ),
                      ),
                    );
                  }
              ),
            )
        )
      ],
    );
  }
}

//마인즈랩
//AI 아바타