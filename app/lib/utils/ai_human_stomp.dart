import 'dart:async';
import 'dart:convert';

import 'package:app/models/stomp/human_stomp.dart';
import 'package:flutter/material.dart';
import 'package:stomp_dart_client/stomp.dart';
import 'package:stomp_dart_client/stomp_config.dart';
import 'package:stomp_dart_client/stomp_frame.dart';

// todo: make class to singleton
class AiHumanStomp2 {
  StreamController _aiHumanController = StreamController<HumanStomp>();
  Stream stream;
  StompClient _stompClient;
  int _projectId;

  //TODO: Error handle
  AiHumanStomp(int projecId) {
    _projectId = projecId;
    _stompClient = StompClient(
        config: StompConfig(
      //TODO: property로 빼기
      url: 'wss://dev-human.maum.ai/api/gs-guide-websocket',
      onConnect: _onConnect,
      beforeConnect: () async {
        print('Waiting to connect ...');
      },
      onWebSocketError: (dynamic error) {
        print('### WEBSOCKET THROW ERROR: ${error}');
        deactivateStomp();
      },
      onStompError: (StompFrame stompFrame) {
        print('### STOPM ERROR: ${stompFrame.body}');
        deactivateStomp();
      },
      onDisconnect: (StompFrame stompFrame) {
        print('### DISCONNECT: ${stompFrame.body}');
      },
      onDebugMessage: (String message) {
        //print('### DEBUG: ${message}');
      },
      onUnhandledMessage: (StompFrame stompFrame) {
        print('### UNHANDLE MESSAGE: ${stompFrame.body}');
      },
    ));
  }

  void _onConnect(StompFrame frame) {
    _stompClient.subscribe(
        destination: '/user/queue/human',
        callback: (frame) {
          HumanStomp result = HumanStomp.fromJson(json.decode(frame.body));
          _aiHumanController.add(result);
        });
    _sendInit();
  }

  void sendHello() {
    _stompClient.send(
        destination: '/app/app/hello',
        body: json.encode({'text':'hello'}));
  }

  void _sendInit() {
    _stompClient.send(
        destination: '/app/app/init',
        body: json.encode({'projectId':_projectId}));
  }

  void sendWav(String data) {
    _stompClient.send(
        destination: '/app/app/audioinput',
        body: json.encode({'audio': data}));
  }

  void sendText(String answer) {
    _stompClient.send(
        destination: '/app/app/textinput', body: json.encode({'text': answer}));
  }

  void sendAutoComplete(String text) {
    _stompClient.send(
      destination: "/app/app/autocomplete", body: json.encode({'text':text}));
  }

  Stream<dynamic> aiHumanListener() {
    stream = _aiHumanController.stream;
    return stream;
  }

  void activateStomp() {
    _stompClient.activate();
  }

  void deactivateStomp() {
    _stompClient.deactivate();
    _aiHumanController.close();
  }
}
