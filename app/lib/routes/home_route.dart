
import 'dart:convert';
import 'dart:io';

import 'package:app/components/auto_complete/auto_complete_main.dart';
import 'package:app/components/dialog/dialog_main.dart';
import 'package:app/components/intent/intent_main.dart';
import 'package:app/components/video_background.dart';
import 'package:app/controllers/auto_complete_controller.dart';
import 'package:app/controllers/avatar_controller.dart';
import 'package:app/controllers/stomp_controller.dart';
import 'package:app/utils/recorder.dart';
import 'package:app/utils/check_internet_status.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

enum PhoneType{
  CELLPHONE, TABLET
}

class HomeRoute extends StatefulWidget {
  final int projectId;
  const HomeRoute({Key key, @required this.projectId}) : super(key: key);

  @override
  _HomeRouteState createState() => _HomeRouteState();
}

//마인즈랩 주소
class _HomeRouteState extends State<HomeRoute> {

  GlobalKey<VideoBackgroundState> _videoBackKey = GlobalKey();
  GlobalKey<DialogMainState> _dialogKey = GlobalKey();

  PhoneType phoneType;
  final AvatarController avatarController = Get.put(AvatarController());
  final AutoCompleteController autoCompleteController = Get.put(AutoCompleteController());
  final StompController aiHumanStomp = Get.put(StompController());
  bool _isShowChat = false;

  @override
  void initState() {
    super.initState();
    aiHumanStomp.initAiHumanStomp(widget.projectId);
    aiHumanListen();
    aiHumanStomp.activateStomp();
  }



  void aiHumanListen() {
    aiHumanStomp.aiHumanListener().listen((humanStomp) {
      if(humanStomp.type == 'STT'){
        if(humanStomp.message != null && humanStomp.message != '') {
          avatarController.addUserChat(humanStomp.message);
        }
      }
      else if (humanStomp.type == 'INIT') {
        _videoBackKey.currentState.loadInitVideo(humanStomp.video, aiHumanStomp.sendHello);
        avatarController.changeInitImage(humanStomp.image);
        avatarController.changeExpectedIntends(humanStomp);
      }
      else if (humanStomp.type == 'ANSWER' || humanStomp.type == 'HELLO' || humanStomp.type == 'ERROR') {
        if(humanStomp.message != null && humanStomp.message != '') {
          avatarController.addBotChat(humanStomp);
          _videoBackKey.currentState.setLoadedVideo(humanStomp.video);
          avatarController.changeExpectedIntends(humanStomp);
        }
      }
      else if(humanStomp.type == 'AUTOCOMPLETE'){
        Get.put(AutoCompleteController()).setAutoCompletes(humanStomp);
      }
      else if((humanStomp.type != null || humanStomp.type != 'AVATAR') && humanStomp.type != 'ERROR'){
        avatarController.addAvatarProcess(humanStomp);
      }
    });
  }

  @override
  void dispose() {
    aiHumanStomp.deactivateStomp();
    super.dispose();
  }

  setPhoneType(Size size){
    if(size.width < 600) phoneType = PhoneType.CELLPHONE;
    else phoneType = PhoneType.TABLET;
  }
//마인즈램
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    setPhoneType(size);

    return Container(
      child: Stack(
        children: [
          VideoBackground(key: _videoBackKey),
          Padding(
            padding: EdgeInsets.only(top: 40, left: 20),
            child: FloatingActionButton(
              heroTag: "SendText",
              child: Icon(Icons.send),
              onPressed: () async {
                await CheckInternet().checkInternetStatus()
                    ? aiHumanStomp.sendTextToStomp(autoCompleteController.textController.text)
                    :CheckInternet().showMyDialog(context); //TODO
              },
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 40, left: 100),
            child: Recorder(),
          ),
          Padding(
            padding: EdgeInsets.only(top: 40, left: 180),
            child: FloatingActionButton(
              heroTag: "showMessage",
              child: Icon(Icons.message),
              onPressed: () {
                setState(() {
                  _isShowChat = !_isShowChat;
                });
              },
            ),
          ),
          Positioned(
            top: (phoneType == PhoneType.CELLPHONE)?size.height*0.13:null,
            right: (phoneType == PhoneType.CELLPHONE)?null:0,
            child: Opacity(
              opacity: _isShowChat?1:0,
              child: Container(
                child: DialogMain(key: _dialogKey, phoneType: phoneType,),
              ),
            ),
          ),
          Positioned(
            child: Container(
              height: 100,
              width: size.width,
              child: IntentMain(),
            ),
            bottom: MediaQuery.of(context).viewInsets.bottom+size.height*0.1,
          ),
          Positioned(
            bottom: MediaQuery.of(context).viewInsets.bottom,
            child: AutoCompleteMain(),
          ),
        ],
      ),
    );
  }
}
