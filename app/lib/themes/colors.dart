import 'package:flutter/material.dart';

Color chatDialogBackgroundColor = const Color.fromRGBO(255, 255, 255, 0.2);
Color chatDialogDateFontColor = const Color.fromRGBO(2, 43, 73, 1);
Color chatDialogBotFontColor = const Color.fromRGBO(2, 43, 73, 1);
Color chatDialogUserFontColor = const Color.fromRGBO(255, 255, 255, 1);
Color dialogTimeFontColor = const Color.fromRGBO(97, 108, 120, 1);
Color chatDialogBotBackgroundColor = const Color.fromRGBO(255, 255, 255, 0.5);
Color chatDialogUserBackgroundColor = const Color.fromRGBO(54, 72, 83, 0.5);
Color processDialogSTTCircleColor = const Color.fromRGBO(254, 162, 106, 1);
Color processDialogSDSCircleColor = const Color.fromRGBO(132, 114, 255, 1);
Color processDialogAvatarCircleColor = const Color.fromRGBO(253, 106, 112, 1);
Color processDialogLineColor = const Color.fromRGBO(255, 255, 255, 0.2);
Color processDialogFontColor = const Color.fromRGBO(2, 43, 73, 1);
Color processDialogTypeFontColor = const Color.fromRGBO(53, 59, 103, 1);