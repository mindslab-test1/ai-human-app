import 'package:app/main.dart';
import 'package:app/resources/app_config.dart';
import 'package:flutter/material.dart';

void main() {
  var configApp = AppConfig(
    appTitle: "Development",
    buildFlavor: "Development",
    child: MyApp(),
  );

  runApp(configApp);
}
