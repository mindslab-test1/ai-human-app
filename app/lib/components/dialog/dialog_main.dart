import 'package:app/components/dialog/AIProcessDialog.dart';
import 'package:app/components/dialog/ChatDialog.dart';
import 'package:app/models/dialog/AIProcessDataDto.dart';
import 'package:app/models/dialog/ChatDialogDataDto.dart';
import 'package:app/models/stomp/human_stomp.dart';
import 'package:app/routes/home_route.dart';
import 'package:flip_card/flip_card.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DialogMain extends StatefulWidget {
  PhoneType phoneType;

  DialogMain({Key key, @required this.phoneType}) : super(key: key);

  @override
  DialogMainState createState() => DialogMainState();
}

class DialogMainState extends State<DialogMain> {
  bool isChat = true;

  @override
  Widget build(BuildContext context) {
    GlobalKey<FlipCardState> cardKey = GlobalKey<FlipCardState>();
    Size size = MediaQuery.of(context).size;
    double chatWidthSize = (widget.phoneType == PhoneType.CELLPHONE)?size.width:size.width*0.575;
    double chatHeightSize = (widget.phoneType == PhoneType.CELLPHONE)?size.height*0.61:null;
    bool isdrag = false;

    return GestureDetector(
      onHorizontalDragEnd: (DragEndDetails dragEndDetails) {
        if(dragEndDetails.velocity.pixelsPerSecond.distance != 0.0)
          cardKey.currentState.toggleCard();
      },
      child: FlipCard(
          key: cardKey,
          flipOnTouch: false,
          front: Column(
            children: [
              Container(
                height: size.height*0.05,
                child: Center(
                  child: Text("chat"),
                ),
              ),
              ChatDialog(
                  width: chatWidthSize,
                  height: chatHeightSize),
            ],
          ),
          back: Column(
            children: [
              Container(
                height: size.height*0.05,
                child: Center(
                  child: Text("process"),
                ),
              ),
              AIProcessDialog(
                  width: chatWidthSize,
                  height: chatHeightSize),
            ],
          )
      ),
    );
  }
}
