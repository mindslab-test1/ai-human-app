import 'package:app/main.dart';
import 'package:app/resources/app_config.dart';
import 'package:flutter/material.dart';

void main() {
  var configApp = AppConfig(
    appTitle: "Production",
    buildFlavor: "Production",
    child: MyApp(),
  );

  runApp(configApp);
}
