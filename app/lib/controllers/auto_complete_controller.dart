import 'package:app/models/stomp/human_stomp.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class AutoCompleteController extends GetxController {
  var autoCompleteList = [].obs;
  var isLoading = true.obs;
  var _textController = TextEditingController();

  setAutoCompletes(HumanStomp humanStomp) {
    if(humanStomp.autoComplete != null) {
      autoCompleteList.value = humanStomp.autoComplete;
      isLoading.value = false;
      update();
    }
  }

  clearTextController() {
    _textController.text = '';
  }

  get textController => _textController;
}