
import 'package:app/routes/home_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/style.dart';
import 'package:get/get.dart';

import '../controllers/avatar_controller.dart';
import 'home_route.dart';


class IntroMain extends StatefulWidget {
  const IntroMain({Key key}) : super(key: key);

  @override
  _IntroMainState createState() => _IntroMainState();
}

class _IntroMainState extends State<IntroMain> {
  final _valueList = [-100, -200, -300, -400];
  var _selectedValue = -100;
  bool _isShowMenu = true;

  List<Widget> stackList = [
    HomeRoute(projectId: -200),
  ];

  changeHomeRoute(){
    setState(() {
      stackList.removeAt(0);
    });
    Get.put(AvatarController()).resetAvatarController();
    addAvatar();
  }

  Future<void> addAvatar() {
    Future.delayed(Duration(seconds: 1)).then((value) => setState(() {
      stackList.add(HomeRoute(projectId: _selectedValue));
    }));
  }

  @override
  Widget build(BuildContext context) {
    final TextEditingController _controller = TextEditingController();
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.menu),
          onPressed: (){
            setState(() {
              _isShowMenu = !_isShowMenu;
            });
          },
        ),
      ),
      body: Stack(
        children: [
          Stack(
            children: stackList,
          ),
          _isShowMenu?Container(
            width: size.width*0.5,
            color: Colors.white.withOpacity(0.7),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                DropdownButton(
                  value: _selectedValue,
                  items: _valueList.map(
                          (value) {
                        return DropdownMenuItem(
                          value: value,
                          child: Text(value.toString(),
                            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                          ),
                        );
                      }
                  ).toList(),
                  onChanged: (value) {
                    setState(() {
                      _selectedValue = value;
                    });
                  },
                ),
                Container(
                  height: 100,
                  width: 300,
                  child: FloatingActionButton(
                    heroTag: "moveToAvatar",
                    child: Text('Click'),
                    onPressed: (){
                      changeHomeRoute();
                    },
                  ),
                ),
              ],
            ),
          ):Container()
        ],
      ),
    );
  }
}
