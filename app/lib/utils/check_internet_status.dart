
import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CheckInternet {
  Future<bool> checkInternetStatus() async{
    var connectivityResult = await (Connectivity().checkConnectivity());
    if(ConnectivityResult.none == connectivityResult) return false;
    else return true;
  }

  Future<void> showMyDialog(BuildContext context) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Internet connect error'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Check your internet connection.'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Ok'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
