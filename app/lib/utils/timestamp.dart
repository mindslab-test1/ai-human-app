import 'package:intl/intl.dart';

class Timestamp{
  static String convertTimestampToDate(int timestamp){
    return DateFormat('yyyy.MM.dd').format(DateTime.fromMillisecondsSinceEpoch(timestamp * 1000));
  }

  static String convertTimestampTotime(int timestamp){
    return DateFormat('H:mm:ss').format(DateTime.fromMillisecondsSinceEpoch(timestamp * 1000));
  }
}