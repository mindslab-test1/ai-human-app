import 'package:app/controllers/avatar_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_html/style.dart';
import 'package:get/get.dart';
import '../../models/dialog/ChatDialogDataDto.dart';
import 'package:app/themes/colors.dart';

class ChatDialog extends StatelessWidget {
  final double width;
  final double height;
  const ChatDialog({Key key, this.width, this.height}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    int currentLength = 0;
    final ScrollController _scrollController = ScrollController();

    return Center(
      child: Container(
        width: width??MediaQuery.of(context).size.width*0.575,
        height: height??MediaQuery.of(context).size.height*0.847,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            // color: chatDialogBackgroundColor
            color: Colors.black.withOpacity(0.5)
        ),
        child: Padding(
          padding: const EdgeInsets.only(right: 8, top: 16, bottom: 16),
          child: Scrollbar(
              isAlwaysShown: true,
              controller: _scrollController,
              radius: Radius.circular(2),
              thickness: 4,
              child: GetBuilder<AvatarController>(
                builder: (avatar) => ListView.builder(
                  itemCount: avatar.chatList.length,
                  itemBuilder: (BuildContext context, int index){
                    if(avatar.chatList.length > 0) {
                      if(avatar.chatList.length > currentLength) {
                        currentLength = avatar.chatList.length;
                        //화면이 다 그려진 후에 실행
                        WidgetsBinding.instance.addPostFrameCallback((
                            timeStamp) {
                          _scrollController.animateTo(
                              _scrollController.position.maxScrollExtent,
                              duration: Duration(milliseconds: 200),
                              curve: Curves.easeInOut);
                        });
                      }
                    }
                    return avatar.chatList[index];
                  },
                  padding: const EdgeInsets.only(right: 8, left: 16),
                  controller: _scrollController,
                ),
              )
          ),
        ),
      ),
    );
  }
}


class DateBlock extends StatelessWidget {
  final String date;
  const DateBlock({Key key, this.date}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Container(
            padding: const EdgeInsets.only(top: 4, bottom: 12),
            child: Text(
              date,
              style: TextStyle(
                  fontFamily: Theme.of(context).textTheme.bodyText1.fontFamily,
                  fontFamilyFallback: Theme.of(context).textTheme.bodyText1.fontFamilyFallback,
                  fontWeight: FontWeight.w500,
                  color: chatDialogDateFontColor,
                  fontSize: 10
              ),
            )
        )
    );
  }
}

class TimeBlock extends StatelessWidget {
  final String time;
  const TimeBlock({Key key, this.time}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      time,
      style: TextStyle(
          fontSize: 10,
          color: dialogTimeFontColor,
          fontFamily: Theme.of(context).textTheme.bodyText1.fontFamily,
          fontFamilyFallback: Theme.of(context).textTheme.bodyText1.fontFamilyFallback,
          fontWeight: FontWeight.w500
      ),
    );
  }
}


class BotDialogBlock extends StatelessWidget {
  final ChatDialogDataDto chatDialogDataDto;
  const BotDialogBlock({Key key, this.chatDialogDataDto}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final AvatarController avatarController = Get.put(AvatarController());

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            CircleAvatar(
              child: (avatarController.initImage.toString() != '')
                  ?Image.network(avatarController.initImage.toString()) // 아바타 이미지
                  :Container(),
              radius: 14,
            ),
            Flexible(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(topLeft: const Radius.circular(20), topRight: const Radius.circular(20), bottomRight: const Radius.circular(20)),
                        color: chatDialogBotBackgroundColor
                    ),
                    margin: const EdgeInsets.symmetric(horizontal: 4),
                    padding: const EdgeInsets.all(12),
                    child: IntrinsicWidth(
                      child: Html(
                        data : chatDialogDataDto.message,
                        shrinkWrap: true,
                        style: {
                          "*": Style(
                              fontSize: const FontSize(12),
                              padding: const EdgeInsets.all(0),
                              margin: const EdgeInsets.all(0),
                              fontFamily: Theme.of(context).textTheme.bodyText1.fontFamily,
                              fontFamilyFallback: Theme.of(context).textTheme.bodyText1.fontFamilyFallback, // todo : 현재 flutter_html library의 style은 fontfamilyfallback을 지원하지 않음, 임의로 fork하여 수정해보았지만 실패
                              color: chatDialogBotFontColor,
                              fontWeight: FontWeight.w500
                          ),
                          "* > strong": Style(
                              fontSize: const FontSize(12),
                              padding: const EdgeInsets.all(0),
                              margin: const EdgeInsets.all(0),
                              fontFamily: Theme.of(context).textTheme.bodyText1.fontFamily,
                              fontFamilyFallback: Theme.of(context).textTheme.bodyText1.fontFamilyFallback,
                              color: chatDialogBotFontColor,
                              fontWeight: FontWeight.w700
                          )
                        },
                      ),
                    ),
                  ),
                  Padding(
                      padding: const EdgeInsets.only(top: 4),
                      child: TimeBlock(time: chatDialogDataDto.time)
                  ),
                ],
              ),
            ),
          ],
        ),
        SizedBox(height: 16,)
      ],
    );
  }
}

class UserDialogBlock extends StatelessWidget {
  final ChatDialogDataDto chatDialogDataDto;

  const UserDialogBlock({Key key, this.chatDialogDataDto}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(topLeft: const Radius.circular(20), topRight: const Radius.circular(20), bottomLeft: const Radius.circular(20)),
                color: chatDialogUserBackgroundColor,
              ),
              margin: EdgeInsets.symmetric(horizontal: 4),
              padding: EdgeInsets.all(12),
              child: Html(
                data : chatDialogDataDto.message,
                shrinkWrap: true,
                style: {
                  "*": Style(
                      fontSize: const FontSize(12),
                      padding: const EdgeInsets.all(0),
                      margin: const EdgeInsets.all(0),
                      fontFamily: Theme.of(context).textTheme.bodyText1.fontFamily,
                      fontFamilyFallback: Theme.of(context).textTheme.bodyText1.fontFamilyFallback,
                      fontWeight: FontWeight.w500,
                      color: chatDialogUserFontColor
                  ),
                  "* > strong": Style(
                      fontSize: const FontSize(12),
                      padding: const EdgeInsets.all(0),
                      margin: const EdgeInsets.all(0),
                      fontFamily: Theme.of(context).textTheme.bodyText1.fontFamily,
                      fontFamilyFallback: Theme.of(context).textTheme.bodyText1.fontFamilyFallback,
                      fontWeight: FontWeight.w700,
                      color: chatDialogUserFontColor
                  )
                },
              ),
            ),
            Padding(
                padding: const EdgeInsets.only(top: 4),
                child: TimeBlock(time: chatDialogDataDto.time)
            ),
            SizedBox(height: 16,)
          ],
        ),
      ],
    );
  }
}