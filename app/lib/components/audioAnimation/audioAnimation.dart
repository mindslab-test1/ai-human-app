import 'package:flutter/material.dart';

class AudioAnimationMain extends StatefulWidget {
  final double height;
  const AudioAnimationMain({Key key, this.height}) : super(key: key);

  @override
  _AudioAnimationMainState createState() => _AudioAnimationMainState();
}

class _AudioAnimationMainState extends State<AudioAnimationMain>
    with TickerProviderStateMixin{

  Animation<double> animation;
  Animation<double> animationTop;
  AnimationController _controller;
  AnimationController _controllerTop;

  @override
  void initState() {
    super.initState();

    _controller = AnimationController(
        duration: Duration(milliseconds: 9000),
        vsync: this)
      ..repeat();
    _controllerTop = AnimationController(
        duration: Duration(milliseconds: 1000),
        vsync: this)
      ..repeat(reverse: true);

    animation = Tween<double>(begin: -2200, end: 2200).animate(_controller);
    animationTop = Tween<double>(begin: widget.height-225, end: widget.height-200).animate(_controllerTop);
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
    _controllerTop.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AudioAnimation(
      animation,
      animationTop
    );
  }
}

class AudioAnimation extends AnimatedWidget {
  final Animation<double> animation;
  final Animation<double> animationTop;

  AudioAnimation(this.animation, this.animationTop)
      : super(listenable: animation);

  final Color backgroundColor = Colors.red;

  List<Widget> getContainers(double width, List<List<int>> rgbList){

    double containerWidth = 900/9;
    List<Widget> containers = [];

    rgbList.forEach((rgb) {
      for(int i =0; i<containerWidth/2;i++){
        double middle = containerWidth/2/2;
        double opacity = ((0.03*(i) + 0.15)>1)?1:(0.03*(i) + 0.15);
        containers.add(Opacity(
          opacity: opacity,
          //opacity: 1,
          child: Container(
            height: 60 -(containerWidth/2-(i)),
            width: 9,
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    stops: [
                      0.1,
                      0.4,
                      0.9
                    ],
                    colors: [
                      Color.fromRGBO(rgb[0],rgb[1],rgb[2], 0),
                      Color.fromRGBO(rgb[0],rgb[1],rgb[2], 0.4),
                      Color.fromRGBO(rgb[0],rgb[1],rgb[2], 1),
                    ]
                )
            ),
          ),
        ));
      }
      for(int i =0; i<containerWidth/2;i++){
        double opacity = ((0.03*(containerWidth/2-i) + 0.15)>1)?1:(0.03*(containerWidth/2-i) + 0.15);
        containers.add(Opacity(
          opacity: opacity,
          child: Container(
              height: (60-i).toDouble(),
              width: 9,
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      stops: [
                        0.1,
                        0.4,
                        0.6,
                        0.8
                      ],
                      colors: [
                        Color.fromRGBO(rgb[0],rgb[1],rgb[2], 0),
                        Color.fromRGBO(rgb[0],rgb[1],rgb[2], 0.4),
                        Color.fromRGBO(rgb[0],rgb[1],rgb[2], 0.8),
                        Color.fromRGBO(rgb[0],rgb[1],rgb[2], 1),
                      ]
                  )
              )),
        ));
      }
    });

    return containers;
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Stack(
      children: [
        Positioned(
          bottom: 0,
          top: animationTop.value,
          child: Container(
            width: size.width,
            height: 300,
            decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Colors.transparent,
                    offset: Offset(0,0)
                  )
                ],
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    stops: [
                      0.0,
                      0.5,
                      0.8
                    ],
                    colors: [
                      Colors.white.withOpacity(0),
                      Color.fromRGBO(184,138,225, 0.5),
                      Color.fromRGBO(184,138,225, 1),
                    ]
                )
            ),
          ),
        ),

        Positioned(
          bottom: 0,
          right: animation.value,
          child: Container(
            width: size.width,
            height: 350,
            child: Container(
              color: Colors.transparent,
              child: OverflowBox(
                maxWidth: 3600,
                maxHeight: 350,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: getContainers(
                      size.width,
                      [
                        [253,148,130],
                        [253,114,166],
                        [184,91,250],
                        [103,106,255]
                      ]
                  ),
                ),
              ),
            ),
          ),
        )
      ],
    );
  }
}