import 'package:get/get.dart';

class StompIdController extends GetxController {
  var projectId = 0.obs;

  setProjectId(int newProjectId) {
    projectId.value = newProjectId;
    update();
  }
}