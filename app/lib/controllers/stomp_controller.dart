import 'dart:async';
import 'dart:convert';

import 'package:app/controllers/auto_complete_controller.dart';
import 'package:app/controllers/avatar_controller.dart';
import 'package:app/models/stomp/human_stomp.dart';
import 'package:get/get.dart';
import 'package:stomp_dart_client/stomp.dart';
import 'package:stomp_dart_client/stomp_config.dart';
import 'package:stomp_dart_client/stomp_frame.dart';

class StompController extends GetxController {
  StreamController _aiHumanController = StreamController<HumanStomp>();
  Stream stream;
  StompClient _stompClient;
  int _projectId;

  //TODO: Error handle
  void initAiHumanStomp(int projecId) {
    _projectId = projecId;
    _stompClient = StompClient(
        config: StompConfig(
          //TODO: property로 빼기
          url: 'wss://dev-human.maum.ai/api/gs-guide-websocket',
          onConnect: _onConnect,
          beforeConnect: () async {
            print('Waiting to connect ...');
          },
          onWebSocketError: (dynamic error) {
            print('### WEBSOCKET THROW ERROR: ${error}');
            deactivateStomp();
          },
          onStompError: (StompFrame stompFrame) {
            print('### STOPM ERROR: ${stompFrame.body}');
            deactivateStomp();
          },
          onDisconnect: (StompFrame stompFrame) {
            print('### DISCONNECT: ${stompFrame.body}');
          },
          onDebugMessage: (String message) {
            //print('### DEBUG: ${message}');
          },
          onUnhandledMessage: (StompFrame stompFrame) {
            print('### UNHANDLE MESSAGE: ${stompFrame.body}');
          },
        ));
  }

  void _onConnect(StompFrame frame) {
    _stompClient.subscribe(
        destination: '/user/queue/human',
        callback: (frame) {
          HumanStomp result = HumanStomp.fromJson(json.decode(frame.body));
          _aiHumanController.add(result);
        });
    _sendInit();
  }

  void sendHello() {
    _stompClient.send(
        destination: '/app/floating/hello',
        body: json.encode({'text':'hello'}));
  }

  void _sendInit() {
    _stompClient.send(
        destination: '/app/floating/init',
        body: json.encode({'projectId':_projectId}));
  }

  void sendWav(String data) {
    _stompClient.send(
        destination: '/app/floating/audioinput',
        body: json.encode({'audio': data}));
  }

  void _sendText(String answer) {
    _stompClient.send(
        destination: '/app/floating/textinput', body: json.encode({'text': answer}));
  }

  void sendAutoComplete(String text) {
    _stompClient.send(
        destination: "/app/floating/autocomplete", body: json.encode({'text':text}));
  }

  Stream<dynamic> aiHumanListener() {
    stream = _aiHumanController.stream;
    return stream;
  }

  void activateStomp() {
    _stompClient.activate();
  }

  void deactivateStomp()  async{
    _stompClient.deactivate();
    _aiHumanController.close();
    _aiHumanController = StreamController<HumanStomp>();
  }

  void sendTextToStomp(String text) {
    if(text != null && text != '') {
      _sendText(text);
      Get.put(AvatarController()).addUserChat(text);
      Get.put(AutoCompleteController()).clearTextController();
      sendAutoComplete('');
    }
  }
}