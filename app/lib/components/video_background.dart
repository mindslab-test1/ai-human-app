import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

class VideoBackground extends StatefulWidget {
  const VideoBackground({Key key}) : super(key: key);

  @override
  VideoBackgroundState createState() => VideoBackgroundState();
}

class VideoBackgroundState extends State<VideoBackground> {
  VideoPlayerController _initController;
  VideoPlayerController _loadController;
  VideoPlayerController _loadController2;
  bool isInitVideoILoaded = false;
  bool isPlayInitVideo = true;
  bool isLoadVideoOneStop = true;
  bool isLoadVideoTwoStop = true;
  double videoHeight = 0;
  double videoWidth = 0;

  @override
  void initState() {
    super.initState();
  }


  @override
  void dispose() {
    super.dispose();
    if(_initController != null) _initController.dispose();
    if(_loadController != null) _loadController.dispose();
    if(_loadController2 != null) _loadController2.dispose();
  }

  Future<void> loadInitVideo(String url, Function callback) async{
    if(_initController == null) {
      _initController = VideoPlayerController.network(url)
        ..setLooping(true)
        ..initialize().then((_) {
          _initController.play();
          setState(() {
            isInitVideoILoaded = true;
            videoHeight = _initController.value.size.height;
            videoWidth = _initController.value.size.width;
          });

          callback();
        });
    }
  }



  Future<void> setLoadedVideo(String url) async {

    if (isLoadVideoOneStop) {
      _loadController = VideoPlayerController.network(url)
        ..initialize().then((_) => _afterInitLoadedVideo())
        ..addListener(() async {
          if (!_loadController.value.isPlaying && !isPlayInitVideo) {
            _initController.play();
            setState(() {
              isPlayInitVideo = true;
            });

            if (!isLoadVideoOneStop && isPlayInitVideo) {
              _loadController.pause();
              setState(() {
                isLoadVideoOneStop = true;
              });
            }
          }
        });
    } else if (isLoadVideoTwoStop) {
      _loadController2 = VideoPlayerController.network(url)
        ..initialize().then((_) => _afterInitLoadedVideo2())
        ..addListener(() async {
          if (!_loadController2.value.isPlaying && !isPlayInitVideo) {
            _initController.play();
            setState(() {
              isPlayInitVideo = true;
            });

            if (!isLoadVideoTwoStop && isPlayInitVideo) {
              _loadController2.pause();
              setState(() {
                isLoadVideoTwoStop = true;
              });
            }
          }
        });
    }
  }

  void _afterInitLoadedVideo() async {
    await _initController.pause();
    await _loadController.play();
    final oldController = _loadController2;
    setState(() {
      isPlayInitVideo = false;
      isLoadVideoOneStop = false;
      isLoadVideoTwoStop = true;
    });
    if(_loadController2 != null) {
      WidgetsBinding.instance.addPostFrameCallback((_) async {
        await oldController.dispose();
      });
      setState(() {
        _loadController2 = null;
      });
    }
  }

  void _afterInitLoadedVideo2() async {
    await _initController.pause();
    await _loadController2.play();
    final oldController = _loadController;
    setState(() {
      isPlayInitVideo = false;
      isLoadVideoTwoStop = false;
      isLoadVideoOneStop = true;
    });
    if(_loadController != null) {
      WidgetsBinding.instance.addPostFrameCallback((_) async {
        await oldController.dispose();
      });
      setState(() {
        _loadController = null;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return OverflowBox(
        maxWidth: double.infinity,
        maxHeight: double.infinity,
        alignment: Alignment.center,
        child: (isInitVideoILoaded)
            ?Container(
                width: videoWidth,
                height: videoHeight,
                child: VideoPlayer(isPlayInitVideo
                    ? _initController
                    : !isLoadVideoOneStop
                        ? _loadController
                      : _loadController2)
            )
            : Container()
        );
  }
}

// 마인즈랩 주소
//AI 아바타
