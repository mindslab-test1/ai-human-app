import 'package:app/utils/permission_utils.dart';
import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';
import 'routes/home_route.dart';
import 'routes.dart';

class MyApp extends StatelessWidget {
  PermissionUtils permissionUtils = PermissionUtils();

  @override
  Widget build(BuildContext context) {
    permissionUtils.checkPermission();

    return GetMaterialApp(
      home: Scaffold(
          body: Container()
      ),
      theme: ThemeData(
        primarySwatch: Colors.blue,
        textTheme: TextTheme(
          bodyText1: TextStyle(
            fontFamily: 'Bronova',
            fontWeight: FontWeight.w500,
            fontFamilyFallback: ['NotoSans']
          ),
          bodyText2: TextStyle(
              fontFamily: 'NotoSans',
              fontWeight: FontWeight.w700,
              fontFamilyFallback: ['Bronova']
          ),
        )
      ),
      initialRoute: "IntroMain",
      getPages: routes,
    );
  }
}
