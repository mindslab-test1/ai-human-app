import 'package:app/controllers/avatar_controller.dart';
import 'package:app/controllers/stomp_controller.dart';
import 'package:app/controllers/stomp_id_controller.dart';
import 'package:app/utils/ai_human_stomp.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';


class IntentMain extends StatelessWidget {
  final Function sendTextToStomp;
  const IntentMain({Key key, @required this.sendTextToStomp}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return GetBuilder<AvatarController>(
      builder: (_avatar) => ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: _avatar.expectedIntentList.length,
        itemBuilder: (context, index) {
          return Center(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  color: Colors.amberAccent,
                  child: TextButton(
                    child: Text(_avatar.expectedIntentList[index]),
                    onPressed: (){
                      Get.put(StompController()).sendTextToStomp(_avatar.expectedIntentList[index]);
                    },
                  ),
                ),
              )
          );
        },
      ),
    );
  }
}

