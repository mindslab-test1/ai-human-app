class HumanStomp {
  String type;
  String message;
  String image;
  String video;
  String debug;
  int date;
  int duration;
  List expectedIntents;
  List<AutoCompleteDto> autoComplete;

  HumanStomp({
    this.type,
    this.message,
    this.image,
    this.video,
    this.debug,
    this.date,
    this.duration,
    this.expectedIntents,
    this.autoComplete
  });

  HumanStomp.fromJson(Map<String, dynamic> json){
    type = json['type'];
    message = json['message'];
    image=json['image'];
    video=json['video'];
    debug=json['debug'];
    date=json['date'];
    duration=json['duration'];
    expectedIntents=json['expectedIntents'];
    if (json['autoComplete'] != null) {
      autoComplete = List<AutoCompleteDto>();
      json['autoComplete'].forEach((v) {
        autoComplete.add(new AutoCompleteDto.fromJson(v));
      });
    }
  }

  @override
  String toString() {
    return 'type: ${type}, message: ${message}, image: ${image}'
        ', video: ${video}, debug: ${debug}, date: ${date}, duration: ${duration}, '
        'expectedIntents:${expectedIntents}, autoComplete:${autoComplete}';
  }
}

class Payload {
  String image;
  String video;
  String message;
  int date;
  int duration;

  Payload({this.image, this.video, this.message, this.date, this.duration});

  Payload.fromJson(Map<String, dynamic> json) {
    image = json['image'];
    video = json['video'];
    message = json['message'];
    date = json['date'];
    duration = json['duration'];
  }

  @override
  String toString() {
    return 'image: ${image}, video: ${video}, message: ${message}, date: ${date}, duration: ${duration}';
  }
}

class AutoCompleteDto {
  String text;
  double score;

  AutoCompleteDto({this.text, this.score});

  AutoCompleteDto.fromJson(Map<String, dynamic> json) {
    text = json['text'];
    score = json['score'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['text'] = this.text;
    data['score'] = this.score;
    return data;
  }

  @override
  String toString() {
    return 'text:$text, score:$score';
  }
}
