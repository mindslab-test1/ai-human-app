import 'dart:convert';

import 'package:app/controllers/stomp_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_audio_recorder/flutter_audio_recorder.dart';
import 'package:get/get.dart';
import 'dart:io' as io;
import 'package:file/file.dart';
import 'package:file/local.dart';

import 'package:path_provider/path_provider.dart';

class RecorderController extends GetxController{
  FlutterAudioRecorder _recorder;
  Recording _current;
  RecordingStatus _currentStatus = RecordingStatus.Unset;
  bool _isRecording = false;

  bool get isRecording => _isRecording;

  set setIsRecording(bool isRecording) => _isRecording = isRecording;

  Future<void> initRecord(BuildContext context) async{
    try {
      if (await FlutterAudioRecorder.hasPermissions) {
        String filePath = "/tmp_audio_recorder";
        io.Directory appDocDirectory;

        if (io.Platform.isIOS) {
          appDocDirectory = await getApplicationDocumentsDirectory();
        } else {
          appDocDirectory = await getExternalStorageDirectory();
        }

        // can add extension like ".mp4" ".wav" ".m4a" ".aac"
        filePath = appDocDirectory.path +
            filePath +
            DateTime.now().millisecondsSinceEpoch.toString();

        _recorder = FlutterAudioRecorder(filePath,
            audioFormat: AudioFormat.WAV, sampleRate: 8000);

        await _recorder.initialized;

        var current = await _recorder.current(channel: 0);

        _current = current;
        _currentStatus = current.status;
      } else {
        Scaffold.of(context).showSnackBar(
            new SnackBar(content: new Text("You must accept permissions")));
      }
    } catch (e) {
      print(e);
    }
  }

  Future<void> startRecord() async {
    try {
      await _recorder.start();
      var recording = await _recorder.current(channel: 0);

      _current = recording;
      _currentStatus = _current.status;
    } catch (e) {
    }
  }

  Future<void> stopRecord(LocalFileSystem localFileSystem) async {
    var result = await _recorder.stop();
    File file = localFileSystem.file(result.path);

    List<int> fileBytes = await file.readAsBytes();
    String base64String = base64Encode(fileBytes);

    Get.put(StompController()).sendWav(base64String);

    await file.delete();

    _current = result;
    _currentStatus = _current.status;
  }
}