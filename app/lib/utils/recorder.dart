import 'package:app/controllers/recorder_controller.dart';
import 'package:file/local.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Recorder extends StatefulWidget {
  final LocalFileSystem localFileSystem;
  Recorder({localFileSystem})
      : this.localFileSystem = localFileSystem ?? LocalFileSystem();

  @override
  State<StatefulWidget> createState() => new _RecorderState();
}

class _RecorderState extends State<Recorder> {
  bool _isRecording = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Get.put(RecorderController()).initRecord(context);
  }

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      child: GetBuilder<RecorderController>(
        builder: (recorder) => Icon(
          Icons.mic,
          color: (recorder.isRecording)?Colors.red:Colors.white,
        ),
      ),

      onPressed: () async{
        bool recordingStatus = Get.put(RecorderController()).isRecording;
        if (!recordingStatus) {
          Get.put(RecorderController()).startRecord();
        } else {
          Get.put(RecorderController()).stopRecord(widget.localFileSystem);

          Get.put(RecorderController()).initRecord(context);
        }

        Get.put(RecorderController()).setIsRecording = !recordingStatus;

        setState(() {
          _isRecording = !_isRecording;
        });
      },
    );
  }
}
