import 'package:app/components/dialog/ChatDialog.dart';
import 'package:app/models/dialog/AIProcessDataDto.dart';
import 'package:app/models/dialog/ChatDialogDataDto.dart';
import 'package:app/models/stomp/human_stomp.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AvatarController extends GetxController {
  var initImage = ''.obs;
  var chatList = [].obs;
  var processList = [].obs;
  var expectedIntentList = [].obs;

  changeInitImage(String newImage){
    initImage = newImage.obs;
    update();
  }

  addUserChat(String text){
    chatList.add(
      UserDialogBlock(
          chatDialogDataDto: ChatDialogDataDto(
              isBot: false,
              message: text,
              timestamp: 1617002899,
              avatarUrl: null
          ),
      )
    );
    update();
  }

  addBotChat(HumanStomp humanStomp) {
    chatList.add(
      BotDialogBlock(
        chatDialogDataDto: ChatDialogDataDto(
            isBot: true,
            message: humanStomp.message,
            timestamp: humanStomp.date,
            avatarUrl: null
        ),
      )
    );
    update();
  }

  addAvatarProcess(HumanStomp humanStomp){
    processList.add(
        AIProcessDataDto(
            duration: humanStomp.duration,
            timestamp: humanStomp.date,
            type: humanStomp.type,
            message: humanStomp.debug.replaceAll('\n', '<br />')
        )
    );
    update();
  }

  changeExpectedIntends(HumanStomp humanStomp) {
    if(humanStomp.expectedIntents != null) {
      expectedIntentList.value = humanStomp.expectedIntents;
      update();
    }
  }

  resetAvatarController() {
    initImage = ''.obs;
    chatList = [].obs;
    processList = [].obs;
    expectedIntentList = [].obs;
  }
}
