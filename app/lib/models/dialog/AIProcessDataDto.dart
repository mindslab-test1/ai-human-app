import 'package:app/utils/timestamp.dart';

class AIProcessDataDto{
  final int duration;
  final int timestamp;
  final String time;
  final String type;
  final AIProcessType aiProcessType;
  final String message;

  AIProcessDataDto({this.duration, this.timestamp, this.message, this.type}) : time = Timestamp.convertTimestampTotime(timestamp), aiProcessType = setAIProcessType(type);
}

AIProcessType setAIProcessType(String type){
  switch (type){
    case "SDS" :
      return AIProcessType.SDS;
      break;
    case "STT" :
      return AIProcessType.STT;
      break;
    case "AVATAR" :
      return AIProcessType.AVATAR;
      break;
    default :
      return null;
  }
}

enum AIProcessType{
  SDS,
  STT,
  AVATAR,
}
