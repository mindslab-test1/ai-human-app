import 'package:app/routes/Intro_route.dart';
import 'package:get/get.dart';
import 'routes/home_route.dart';
import 'package:app/components/dialog/ChatDialog.dart';

final List<GetPage> routes = [
  //GetPage(name: "HomeRoute", page: () => HomeRoute()),
  GetPage(name: "chatting", page: () => ChatDialog()),
  GetPage(name: "IntroMain", page: () => IntroMain())
];
