import 'package:permission_handler/permission_handler.dart';

class PermissionUtils {
  static final PermissionUtils _permissionUtils = new PermissionUtils._internal();
  PermissionStatus _storagePermission;

  PermissionUtils._internal() {
    _storagePermission = null;
  }

  factory PermissionUtils() => _permissionUtils;

  PermissionStatus get storagePermission => _storagePermission;

  Future<void> checkPermission() async {
    PermissionStatus storage = await Permission.storage.status;
    if(!storage.isGranted){
      Map<Permission, PermissionStatus> statuses = await [
        Permission.storage
      ].request();

      storage = statuses[Permission.storage];
    }

    _storagePermission = storage;
  }
}
