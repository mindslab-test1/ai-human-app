import 'package:app/utils/timestamp.dart';
// todo : move directory to appropriate directory not under component
class ChatDialogDataDto {
  String avatarUrl;
  bool isBot;
  String message;
  int timestamp;
  String time;
  String date;

  ChatDialogDataDto({this.avatarUrl, this.isBot, this.message, this.timestamp}){
    //this.date = Timestamp.convertTimestampToDate(this.timestamp);
    //this.time = Timestamp.convertTimestampTotime(this.timestamp);
    this.date = '17:19:47';
    this.time = '17:19:47';
  }

  @override
  String toString() {
    return 'avatarUrl:$avatarUrl, isBot:$isBot, message:$message, '
        'timestamp:$timestamp, time:$time, date:$date';
  }
}