import 'package:app/controllers/avatar_controller.dart';
import 'package:app/models/dialog/AIProcessDataDto.dart';
import 'package:app/themes/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_html/style.dart';
import 'package:get/get.dart';

class AIProcessDialog extends StatelessWidget {
  final double width;
  final double height;
  final ScrollController _scrollController = ScrollController();

  AIProcessDialog({Key key, this.width, this.height}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Center(
      child: Container(
        width: width??MediaQuery.of(context).size.width*0.575,
        height: height??MediaQuery.of(context).size.height*0.847,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: Colors.black.withOpacity(0.5)
        ),
        child: Padding(
          padding: const EdgeInsets.only(right: 8, top: 16, bottom: 16),
          child: Scrollbar(
            isAlwaysShown: true,
            controller: _scrollController,
            radius: Radius.circular(2),
            thickness: 4,
            child: GetBuilder<AvatarController>(
              builder: (avatar) => ListView.builder(
                shrinkWrap: true,
                padding: const EdgeInsets.only(right: 8, left: 16),
                controller: _scrollController,
                itemCount: avatar.processList.length??0,
                itemBuilder: (BuildContext context, int index){
                  return AIProcessDialogBlock(
                    data: avatar.processList[index],
                    isLastData: (avatar.processList.length == (index + 1)),
                  );
                },
              )
            ),
          ),
        ),
      ),
    );
  }
}


class AIProcessDialogBlock extends StatelessWidget {
  final AIProcessDataDto data;
  final bool isLastData;
  const AIProcessDialogBlock({Key key, this.data, this.isLastData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IntrinsicHeight(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ProcessLineBlock(isLastData: isLastData, aiProcessType: data.aiProcessType,),
          SizedBox(width: 12,),
          TextDialogBlock(aiProcessDataDto: data,)
        ],
      ),
    );
  }
}

class ProcessLineBlock extends StatelessWidget {
  final bool isLastData;
  final AIProcessType aiProcessType;
  const ProcessLineBlock({Key key, this.isLastData, this.aiProcessType}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Color circleColor;
    switch (this.aiProcessType){
      case AIProcessType.AVATAR:
        circleColor = processDialogAvatarCircleColor;
        break;
      case AIProcessType.SDS:
        circleColor = processDialogSDSCircleColor;
        break;
      case AIProcessType.STT:
        circleColor = processDialogSTTCircleColor;
        break;
      default:
        circleColor = Colors.black;
    }
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          decoration: BoxDecoration(
              shape: BoxShape.circle,
              border: Border.all(width: 5,color: circleColor),
              color: Colors.transparent
          ),
          child: Container(
            width: 12,
            height: 12,
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.transparent
            ),
          ),
        ),
        if(!isLastData) Expanded(
            child: VerticalDivider(thickness: 2, color: processDialogLineColor)
        )
      ],
    );
  }
}


class TextDialogBlock extends StatelessWidget {
  final AIProcessDataDto aiProcessDataDto;

  const TextDialogBlock({Key key, this.aiProcessDataDto}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 2,),
          ProcessTypeBlock(type: aiProcessDataDto.type,),
          SizedBox(height: 4,),
          ProcessBodyBlock(message: aiProcessDataDto.message,),
          SizedBox(height: 4,),
          Align(
            alignment: Alignment.centerRight,
            child: ProcessTimeBlock(time:aiProcessDataDto.time, duration: aiProcessDataDto.duration??null, ),
          ),
          SizedBox(height: 16,)
        ],
      ),
    );
  }
}

class ProcessBodyBlock extends StatelessWidget {
  final String message;

  const ProcessBodyBlock({Key key, this.message}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Html(
      data : message,
      shrinkWrap: true,
      style: {
        "*": Style(
            fontSize: const FontSize(16),
            padding: const EdgeInsets.all(0),
            margin: const EdgeInsets.all(0),
            fontFamily: Theme.of(context).textTheme.bodyText2.fontFamily,
            fontFamilyFallback: Theme.of(context).textTheme.bodyText1.fontFamilyFallback, // todo : 현재 flutter_html library의 style은 fontfamilyfallback을 지원하지 않음, 임의로 fork하여 수정해보았지만 실패
            color: processDialogFontColor,
            fontWeight: FontWeight.w300
        ),
        "* > strong": Style(
            fontSize: const FontSize(16),
            padding: const EdgeInsets.all(0),
            margin: const EdgeInsets.all(0),
            fontFamily: Theme.of(context).textTheme.bodyText2.fontFamily,
            fontFamilyFallback: Theme.of(context).textTheme.bodyText1.fontFamilyFallback,
            color: processDialogFontColor,
            fontWeight: FontWeight.w500
        )
      },
    );
  }
}


class ProcessTypeBlock extends StatelessWidget {
  final String type;
  const ProcessTypeBlock({Key key, this.type}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      type.toString(),
      style: TextStyle(
          fontSize: 20,
          fontFamily: Theme.of(context).textTheme.bodyText1.fontFamily,
          fontWeight: Theme.of(context).textTheme.bodyText1.fontWeight,
          color: processDialogTypeFontColor
      ),
    );
  }
}

class ProcessTimeBlock extends StatelessWidget {
  final int duration;
  final String time;

  const ProcessTimeBlock({Key key, this.duration, this.time}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
        duration==0 ? time : duration.toString()+ "sec / " + time.toString(),
        style: TextStyle(
            fontSize: 12,
            fontFamily: Theme.of(context).textTheme.bodyText1.fontFamily,
            fontWeight: FontWeight.w500,
            color: dialogTimeFontColor
        )
    );
  }
}
